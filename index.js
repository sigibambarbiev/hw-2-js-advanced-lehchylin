//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//Если при выполнении программы в скрипте возникнет ошибка, то он остановится и прекратит выполнять код. Скрипт "упадет". Например, если пользователь введет некорректные данные, программа просто остановится, но если использовать конструкцию try...catch, то можно отловить эту ошибку и предложить пользователю ввести данные заново или выполнить что-либо еще. Таким образом, скрипт не упадет и можно будет продолжить его работу.

import {books} from "./js/books.js"
import {etalonObj} from "./js/etalon.js";

const list = document.querySelector('#root');
const createUlElem = document.createElement('ul');
list.append(createUlElem);

function checkBooks(array, etalon) {

    array.forEach(obj => {

        try {
            let createTitle;
            let createElem;

            for (const objKey in etalon) {

                if (!obj.hasOwnProperty(objKey)) {
                    throw new Error(`в книзі ${JSON.stringify(obj)} відсутня властивість ${objKey}`);
                }

            }

            createTitle = document.createElement('p');
            createUlElem.append(createTitle);
            createTitle.innerText = 'Book';

            for (const objKey in obj) {
                createElem = document.createElement('li');
                createTitle.append(createElem);
                createElem.innerText = objKey + '--' + obj[objKey];
            }

        } catch (err) {
            console.error('incomplete data:', err.message);
        }
    })
}

checkBooks(books, etalonObj);